# GLSL

## 数据类型

> GLSL中包含C语言大部分的默认基础数据类型：`int`、`float`、`double`、`uint`和`bool`。GLSL还有两种容器类型：向量和矩阵

### 向量

> GLSL中向量可包含2、3或4个分量，分量的类型可以前面默认基础类型中的任意一个，向量有以下几种形式（`n`表示分量的数量）

| 类型      | 含义                        |
| ------- | ------------------------- |
| `vecn`  | 包含`n`个float分量的默认向量        |
| `bvecn` | 包含`n`个bool分量的默认向量         |
| `ivecn` | 包含`n`个int分量的默认向量          |
| `uvecn` | 包含`n`个unsigned int分量的默认向量 |
| `dvecn` | 包含`n`个double分量的默认向量       |

大多数时候我们都用`vecn`

---

向量的分量可以通过`vec.x`获取，`.x`、`.y`、`.z`、`.w`分别表示第1、2、3、4个分量

向量的**重组**

```glsl
vec2 avec;
vec4 ovec = avec.xyyx;
vec3 anotherVec = ovec.zyw;
vec4 lastVec = anotherVec.xxzw - avec.yxyy;
```

### 矩阵

TODO

## 输入与输出

> GLSL和C语言的语法很像，虽然着色器是各自独立的小程序，但它们都是整体的一部分，因此每个着色器都可以有输入和输出，在OpenGL里是`in`和`out`，在OpenGL ES里只有一个：`varying`

例如：

* 顶点着色器
  
  ```glsl
  attribute vec3 pos;
  attribute vec3 _color;
  varying vec3 color;
  
  void main()
  {
      gl_Position = vec4(pos, 1.0);
      color = _color;
  }
  ```

* 片段着色器
  
  ```glsl
  varying vec3 color;
  
  void main()
  {
      gl_FragColor = vec4(color, 1.0);
  }
  ```

通过以上代码，就可以为每个顶点设置一种颜色，用`varying`声明的变量的值会在两个着色器之间传递（**注意：<mark>两个着色器中变量的名称必须一致</mark>，否则无法传递**）

### 细节

> 用`attribute`声明的变量叫做<mark>顶点属性</mark>（*<u>事实上，在顶点着色器中声明的输入变量都叫这个</u>*），我们能声明的顶点属性是有上限的，一般由硬件决定，我们可以用以下代码来查询
> 
> ```c
> int nrAttributes;
> glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
> printf("%d\n", nrAttributes);
> ```

## 从CPU中的程序向着色器发送数据

> 可以在CPU应用中向GPU中着色器发送数据的有两种方式：`uniform`和`attribute`

### Uniform

> `uniform`是<mark>全局</mark>的，这意味着`uniform`变量必须在每个着色器程序对象中都是独一无二的，而且它可以<u>被着色器程序中的任意着色器在任意阶段访问</u>
> 
> 无论你把`uniform`值设置成什么，<u>`uniform`会一直保存，直到被重置或更新</u>
> 
> `uniform`一次只能输入一个数据（*也就是一个顶点或一个颜色*），在代码中用`glUniform`开头的函数来输入，例如`glUniform3f`表示输入给`vec3`的变量

### Attribute

> `attribute`与`uniform`不同之处在于它<u>一次可以输入多个数据</u>（*即一次可以传递一组顶点或一组颜色*），但它<mark>只能用于顶点着色器</mark>
> 
> **在代码中首先需要用`glEnableVertexAttribArray`来启用，否则GPU无法读取数据，输入数据则可以使用`glVertexAttribPointer`**
